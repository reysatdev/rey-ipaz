@extends('layouts.app')

@section('content')

<div class="row mt-5">
    <div class="col-lg-6">
        <h2>Variants</h2>
    </div>
    <div class="col-lg-6">
        <div class="text-end">
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#createVariantModal">
                Tambah Baru
            </button>
        </div>
    </div>
</div>

<div class="mt-5">
    @if ($message = Session::get('success'))
    <div class="alert alert-primary" role="alert">
        {{ $message }}
    </div>
    @endif

    @if ($message = Session::get('error'))
    <div class="error">
        {{ $message }}
    </div>
    @endif

    @if($errors->any())
    <div class="error">
        @foreach($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
    @endif

    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nama</th>
                <th class="hide-hp">DiTambah</th>
                <th class="hide-hp">Diperbaharui</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @if ($variants->isEmpty())
            <tr>
                <td colspan="5" style="text-align: center;">Belum Ada Data</td>
            </tr>
            @else
            @foreach ($variants as $variant)
            <tr>
                <td>{{ $variant->id }}</td>
                <td>{{ $variant->nama }}</td>
                <td class="hide-hp">{{ $variant->created_at->format('j M Y - H:i') }}</td>
                <td class="hide-hp">{{ $variant->updated_at->format('j M Y - H:i') }}</td>
                <td>
                    <form action="{{ route('variants.destroy', $variant->id) }}" method="POST">
                        <a class="btn btn-primary" data-bs-toggle="modal"
                            data-bs-target="#editVariantModal-{{ $variant->id }}">Edit</a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Hapus</button>
                    </form>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>

@if ($variants->isNotEmpty())
@foreach ($variants as $variant)
<!-- Edit Variant Modal -->
<div class="modal fade" id="editVariantModal-{{ $variant->id }}" tabindex="-1"
    aria-labelledby="editVariantModalLabel-{{ $variant->id }}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editVariantModalLabel-{{ $variant->id }}">Edit Varian - {{ $variant->nama }}
                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('variants.update', $variant->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                        <label for="name" class="form-label">Nama</label>
                        <input type="text" class="form-control" id="name" name="nama" value="{{ $variant->nama }}" placeholder="Masukan nama varian" oninvalid="this.setCustomValidity('Nama varian wajib diisi')" oninput="this.setCustomValidity('')" required>
                    </div>
                    <!-- Add more inputs as needed -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach
@endif

<!-- Create Variant Modal -->
<div class="modal fade" id="createVariantModal" tabindex="-1" aria-labelledby="createVariantModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createVariantModalLabel">Tambah Varian</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('variants.store') }}" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label for="name" class="form-label">Nama</label>
                        <input type="text" class="form-control" id="name" name="nama" placeholder="Masukan nama varian" oninvalid="this.setCustomValidity('Nama varian wajib diisi')" oninput="this.setCustomValidity('')" required>
                    </div>
                    <!-- Add more inputs as needed -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection