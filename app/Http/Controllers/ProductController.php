<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Variants;
use App\Models\Image;

class ProductController extends Controller
{
    public function index()
    {
        $pageTitle = 'Product | REY IPAZ SAMPLE';
        $product = Product::with('category', 'variant', 'gambar')->get();
        $kategori = Category::all();
        $varian = Variants::all();
        $gambar = Image::all();
        return view('product.index', compact('pageTitle', 'product', 'kategori', 'varian', 'gambar'));
    }

    public function create()
    {
        return view('product.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'img' => 'required',
            'harga' => 'required',
            'cat' => 'required',
            'var' => 'required',
        ], [
            'nama.required' => 'Nama harus diisi.',
            'img.required' => 'Gambar harus diisi.',
            'harga.required' => 'Harga harus diisi.',
            'cat.required' => 'Kategori harus diisi.',
            'var.required' => 'Varian harus diisi.',
        ]);

        $product = new Product();
        $product->nama = $request->input('nama');
        $product->img = $request->input('img');
        $product->harga = $request->input('harga');
        $product->category_id = $request->input('cat');
        $product->variant_id = $request->input('var');

        $product->save();

        return redirect()->route('product.index')
            ->with('success', 'Product berhasil dibuat.');
    }

    public function edit(Product $product)
    {
        return view('product.edit', compact('Product'));
    }

    public function update(Request $request, Product $product)
    {
        $request->validate([
            'nama' => 'required',
            'img' => 'required',
            'harga' => 'required',
            'cat' => 'required',
            'var' => 'required',
        ], [
            'nama.required' => 'Nama harus diisi.',
            'img.required' => 'Gambar harus diisi.',
            'harga.required' => 'Harga harus diisi.',
            'cat.required' => 'Kategori harus diisi.',
            'var.required' => 'Varian harus diisi.',
        ]);

        $product->nama = $request->input('nama');
        $product->img = $request->input('img');
        $product->harga = $request->input('harga');
        $product->category_id = $request->input('cat');
        $product->variant_id = $request->input('var');

        $product->save();

        return redirect()->route('product.index')
            ->with('success', 'Product berhasil diperbarui.');
    }

    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('product.index')
            ->with('success', 'Product berhasil dihapus.');
    }

    public function show($id)
    {
        $product = Product::with('category', 'variant', 'gambar')->findOrFail($id);
        $pageTitle = $product->nama . ' | REY IPAZ SAMPLE';

        // Mengedit data sebelum ditampilkan
        $image = $product->gambar->file ?? null;
        $product->img = $image ? url('img/' . $image) : null;

        return view('product.show', compact('pageTitle', 'product'));
    }

}