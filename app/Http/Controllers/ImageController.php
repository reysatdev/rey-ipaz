<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Image;
use App\Models\Product;

class ImageController extends Controller
{
    public function index()
    {
        $pageTitle = 'Gambar | REY IPAZ SAMPLE';
        $images = Image::all();
        return view('images.index', compact('pageTitle', 'images'));
    }

    public function create()
    {
        return view('images.create');
    }

    public function store(Request $request)
    {

        $request->validate([
            'nama' => 'required',
            'file' => 'required|image',
        ], [
            'nama.required' => 'Nama harus diisi.',
            'file.required' => 'File harus diisi.',
        ]);

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('img'), $filename);

            $image = Image::create([
                'nama' => $request->nama,
                'file' => $filename,
            ]);

            return redirect()->route('images.index')->with('success', 'Gambar berhasil ditambahkan.');
        }

        return redirect()->back()->withErrors(['file' => 'Gagal mengunggah gambar.']);
    }

    public function show($id)
    {
        $image = Image::find($id);
        return view('images.show', compact('image'));
    }

    public function edit($id)
    {
        $image = Image::find($id);
        return view('images.edit', compact('image'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
        ], [
            'nama.required' => 'Nama harus diisi.',
        ]);

        $image = Image::find($id);

        if ($request->hasFile('file')) {
            // Menghapus file gambar lama dari direktori
            $oldImagePath = public_path('img') . '/' . $image->file;
            if (file_exists($oldImagePath)) {
                unlink($oldImagePath);
            }

            // Mengupload dan menyimpan file gambar baru
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('img'), $filename);
            $image->file = $filename;
        }

        $image->nama = $request->nama;
        $image->save();

        return redirect()->route('images.index')->with('success', 'Gambar berhasil diperbarui.');
    }

    public function destroy($id)
    {
        $image = Image::find($id);

        $isDependent = Product::where('img', $image->id)->exists();
        if ($isDependent) {
            return redirect()->route('images.index')->with('error', 'Tidak dapat menghapus gambar karena ada produk yang terkait.');
        }

        // Menghapus file gambar dari direktori
        $imagePath = public_path('img') . '/' . $image->file;
        if (file_exists($imagePath)) {
            unlink($imagePath);
        }

        // Menghapus record gambar dari database
        $image->delete();

        return redirect()->route('images.index')->with('success', 'Gambar berhasil dihapus.');
    }
}