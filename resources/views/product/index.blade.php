@extends('layouts.app')

@section('content')

<div class="row mt-5">
    <div class="col-lg-6">
        <h2>Product</h2>
    </div>
    <div class="col-lg-6">
        <div class="text-end">
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#createProductModal">
                Tambah Baru
            </button>
        </div>
    </div>
</div>

<div class="mt-5">
    @if ($message = Session::get('success'))
    <div class="alert alert-primary" role="alert">
        {{ $message }}
    </div>
    @endif

    @if ($message = Session::get('error'))
    <div class="error">
        {{ $message }}
    </div>
    @endif

    @if($errors->any())
    <div class="error">
        @foreach($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
    @endif

    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nama</th>
                <th class="hide-hp">Gambar</th>
                <th class="hide-hp">Kategori</th>
                <th class="hide-hp">Varian</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @if ($product->isEmpty())
            <tr>
                <td colspan="6" style="text-align: center;">Belum Ada Data</td>
            </tr>
            @else
            @foreach ($product as $pro)
            <tr>
                <td>{{ $pro->id }}</td>
                <td><a href="{{ url('/api/products/' . $pro->id) }}">{{ $pro->nama }}</a></td>
                <td>
                    <div class="rey-img"><img src="/img/{{ $pro->gambar->file }}" alt="{{ $pro->gambar->nama }}"
                            loading="eager"></div>
                </td>
                <td>{{ $pro->category->nama }}</td>
                <td>{{ $pro->variant->nama }}</td>
                <td>
                    <form action="{{ route('product.destroy', $pro->id) }}" method="POST">
                        <a class="btn btn-primary" data-bs-toggle="modal"
                            data-bs-target="#editProductModal-{{ $pro->id }}">Edit</a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Hapus</button>
                    </form>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>

@if ($product->isNotEmpty())
@foreach ($product as $pro)
<!-- Edit Product Modal -->
<div class="modal fade" id="editProductModal-{{ $pro->id }}" tabindex="-1"
    aria-labelledby="editProductModalLabel-{{ $pro->id }}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editProductModalLabel-{{ $pro->id }}">Edit Product - {{ $pro->nama }}
                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('product.update', $pro->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                        <label for="name" class="form-label">Nama</label>
                        <input type="text" class="form-control" id="name" name="nama"
                            placeholder="Masukan nama product" oninvalid="this.setCustomValidity('Nama product wajib diisi')"
                            oninput="this.setCustomValidity('')" required value="{{ $pro->nama }}">
                    </div>
                    <div class="mb-3">
                        @if ($gambar->isEmpty())
                        Tambahkan Dulu Gambar <a href="{{ url('/images') }}">Klik Disini</a>
                        @else
                        <label for="img" class="form-label">Gambar</label>
                        <select class="select2" name="img" id="img" required>
                            <option value="" hidden>Pilih Gambar</option>
                            @foreach ($gambar as $img)
                            <option value="{{ $img->id }}" data-thumbnail="{{ url('/img/' . $img->file) }}" @if ($img->id == $pro->img) selected @endif>{{ $img->nama }}</option>
                            @endforeach
                        </select>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="harga" class="form-label">Harga</label>
                        <input type="number" class="form-control" id="harga" name="harga"
                            placeholder="Masukan harga product" oninvalid="this.setCustomValidity('Harga product wajib diisi')"
                            oninput="this.setCustomValidity('')" required value="{{ $pro->harga }}">
                    </div>
                    <div class="mb-3">
                        @if ($kategori->isEmpty())
                        Tambahkan Dulu Kategori <a href="{{ url('/category') }}">Klik Disini</a>
                        @else
                        <label for="cat" class="form-label">Kategori</label>
                        <select class="form-control" name="cat" id="cat"
                            oninvalid="this.setCustomValidity('Kategori wajib diisi')" oninput="this.setCustomValidity('')"
                            required>
                            <option value="" hidden>Pilih Kategori</option>
                            @foreach ($kategori as $kat)
                            <option value="{{ $kat->id }}"
                                @if ($kat->id == $pro->category_id) selected @endif>{{ $kat->nama }}</option>
                            @endforeach
                        </select>
                        @endif
                    </div>
                    <div class="mb-3">
                        @if ($varian->isEmpty())
                        Tambahkan Dulu Varian <a href="{{ url('/variants') }}">Klik Disini</a>
                        @else
                        <label for="var" class="form-label">Varian</label>
                        <select class="form-control" name="var" id="var"
                            oninvalid="this.setCustomValidity('Varian wajib diisi')" oninput="this.setCustomValidity('')"
                            required>
                            <option value="" hidden>Pilih Varian</option>
                            @foreach ($varian as $var)
                            <option value="{{ $var->id }}"
                                @if ($var->id == $pro->variant_id) selected @endif>{{ $var->nama }}</option>
                            @endforeach
                        </select>
                        @endif
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach
@endif

<!-- Create Product Modal -->
<div class="modal fade" id="createProductModal" tabindex="-1" aria-labelledby="createProductModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createProductModalLabel">Tambah Product</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('product.store') }}" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label for="name" class="form-label">Nama</label>
                        <input type="text" class="form-control" id="name" name="nama" placeholder="Masukan nama product" oninvalid="this.setCustomValidity('Nama product wajib diisi')" oninput="this.setCustomValidity('')" required>
                    </div>
                    <div class="mb-3">
                        @if ($gambar->isEmpty())
                        Tambahkan Dulu Gambar <a href="{{ url('/images') }}">Klik Disini</a>
                        @else
                        <label for="img" class="form-label">Gambar</label>
                        <select class="select2" name="img" id="img" required>
                            <option value="" hidden>Pilih Gambar</option>
                            @foreach ($gambar as $img)
                            <option value="{{ $img->id }}" data-thumbnail="{{ url('/img/' . $img->file) }}">{{ $img->nama }}
                            </option>
                            @endforeach
                        </select>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="harga" class="form-label">Harga</label>
                        <input type="number" class="form-control" id="harga" name="harga" placeholder="Masukan harga product" oninvalid="this.setCustomValidity('harga product wajib diisi')" oninput="this.setCustomValidity('')" required>
                    </div>
                    <div class="mb-3">
                        @if ($kategori->isEmpty())
                        Tambahkan Dulu Kategori <a href="{{ url('/category') }}">Klik Disini</a>
                        @else
                        <label for="cat" class="form-label">Kategori</label>
                        <select class="form-control" name="cat" id="cat" oninvalid="this.setCustomValidity('Kategori wajib diisi')" oninput="this.setCustomValidity('')" required>
                            <option value="" hidden>Pilih Kategori</option>
                            @foreach ($kategori as $kat)
                            <option value="{{ $kat->id }}">{{ $kat->nama }}</option>
                            @endforeach
                        </select>
                        @endif
                    </div>
                    <div class="mb-3">
                        @if ($varian->isEmpty())
                        Tambahkan Dulu Varian <a href="{{ url('/variants') }}">Klik Disini</a>
                        @else
                        <label for="var" class="form-label">Varian</label>
                        <select class="form-control" name="var" id="var" oninvalid="this.setCustomValidity('Nama kategori wajib diisi')" oninput="this.setCustomValidity('')" required>
                            <option value="" hidden>Pilih Varian</option>
                            @foreach ($varian as $var)
                            <option value="{{ $var->id }}">{{ $var->nama }}</option>
                            @endforeach
                        </select>
                        @endif
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function() {
  $('.select2').each(function(index) {
    var $select = $(this);
    var selectId = 'select2-' + (index + 1);
    $select.attr('id', selectId);

    $select.select2({
      templateResult: formatSelectOption,
      templateSelection: formatSelectOption,
      width: '100%',
    });
  });
});

function formatSelectOption(option) {
  if (!option.id) {
    return option.text;
  }

  var thumbnailUrl = $(option.element).data('thumbnail');
  var $option = $('<span><img src="' + thumbnailUrl + '" class="select-thumbnail" /> ' + option.text + '</span>');
  return $option;
}
</script>

@endsection