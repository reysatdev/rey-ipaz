@extends('layouts.app')

@section('content')

<div class="row mt-5">
    <div class="col-lg-6">
        <h2>Kategori</h2>
    </div>
    <div class="col-lg-6">
        <div class="text-end">
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#createKategoritModal">
                Tambah Baru
            </button>
        </div>
    </div>
</div>

<div class="mt-5">
    @if ($message = Session::get('success'))
    <div class="alert alert-primary" role="alert">
        {{ $message }}
    </div>
    @endif

    @if ($message = Session::get('error'))
    <div class="error">
        {{ $message }}
    </div>
    @endif

    @if($errors->any())
    <div class="error">
        @foreach($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
    @endif

    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nama</th>
                <th class="hide-hp">DiTambah</th>
                <th class="hide-hp">Diperbaharui</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @if ($categories->isEmpty())
            <tr>
                <td colspan="5" style="text-align: center;">Belum Ada Data</td>
            </tr>
            @else
            @foreach ($categories as $cat)
            <tr>
                <td>{{ $cat->id }}</td>
                <td>{{ $cat->nama }}</td>
                <td class="hide-hp">{{ $cat->created_at->format('j M Y - H:i') }}</td>
                <td class="hide-hp">{{ $cat->updated_at->format('j M Y - H:i') }}</td>
                <td>
                    <form action="{{ route('category.destroy', $cat->id) }}" method="POST">
                        <a class="btn btn-primary" data-bs-toggle="modal"
                            data-bs-target="#editKategoritModal-{{ $cat->id }}">Edit</a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Hapus</button>
                    </form>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>

@foreach ($categories as $cat)
<!-- Edit Kategorit Modal -->
<div class="modal fade" id="editKategoritModal-{{ $cat->id }}" tabindex="-1"
    aria-labelledby="editKategoritModalLabel-{{ $cat->id }}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editKategoritModalLabel-{{ $cat->id }}">Edit Kategori - {{ $cat->nama }}
                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('category.update', $cat->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                        <label for="name" class="form-label">Nama</label>
                        <input type="text" class="form-control" id="name" name="nama" value="{{ $cat->nama }}" placeholder="Masukan nama kategori" oninvalid="this.setCustomValidity('Nama kategori wajib diisi')" oninput="this.setCustomValidity('')" required>
                    </div>
                    <!-- Add more inputs as needed -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach

<!-- Create Kategorit Modal -->
<div class="modal fade" id="createKategoritModal" tabindex="-1" aria-labelledby="createKategoritModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createKategoritModalLabel">Tambah Kategori</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('category.store') }}" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label for="name" class="form-label">Nama</label>
                        <input type="text" class="form-control" id="name" name="nama" placeholder="Masukan nama kategori" oninvalid="this.setCustomValidity('Nama kategori wajib diisi')" oninput="this.setCustomValidity('')" required>
                    </div>
                    <!-- Add more inputs as needed -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection