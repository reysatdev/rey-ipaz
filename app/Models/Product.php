<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['nama', 'img', 'harga', 'category_id', 'variant_id'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function variant()
    {
        return $this->belongsTo(Variants::class, 'variant_id');
    }

    public function gambar()
    {
        return $this->belongsTo(Image::class, 'img');
    }
    
}