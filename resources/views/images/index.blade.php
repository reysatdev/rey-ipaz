@extends('layouts.app')

@section('content')

<div class="row mt-5">
    <div class="col-lg-6">
        <h2>Gambar</h2>
    </div>
    <div class="col-lg-6">
        <div class="text-end">
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#createGambartModal">
                Tambah Baru
            </button>
        </div>
    </div>
</div>

<div class="mt-5">
    @if ($message = Session::get('success'))
    <div class="alert alert-primary" role="alert">
        {{ $message }}
    </div>
    @endif

    @if ($message = Session::get('error'))
    <div class="error">
        {{ $message }}
    </div>
    @endif

    @if($errors->any())
    <div class="error">
        @foreach($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
    @endif

    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nama</th>
                <th>Gambar</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @if ($images->isEmpty())
            <tr>
                <td colspan="4" style="text-align: center;">Belum Ada Data</td>
            </tr>
            @else
            @foreach ($images as $img)
            <tr>
                <td>{{ $img->id }}</td>
                <td>{{ $img->nama }}</td>
                <td>
                    <div class="rey-img"><img src="{{ url('/img/'.$img->file) }}" alt="{{ $img->nama }}"
                            loading="eager"></div>
                </td>
                <td>
                    <form action="{{ route('images.destroy', $img->id) }}" method="POST">
                        <a class="btn btn-primary" data-bs-toggle="modal"
                            data-bs-target="#editGambartModal-{{ $img->id }}">Edit</a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Hapus</button>
                    </form>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>

@if ($images->isNotEmpty())
@foreach ($images as $img)
<!-- Edit Gambart Modal -->
<div class="modal fade" id="editGambartModal-{{ $img->id }}" tabindex="-1"
    aria-labelledby="editGambartModalLabel-{{ $img->id }}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editGambartModalLabel-{{ $img->id }}">Edit Gambar - {{ $img->nama }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('images.update', $img->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                        <label for="name" class="form-label">Nama</label>
                        <input type="text" class="form-control" id="name" name="nama" value="{{ $img->nama }}" placeholder="Masukan nama gambar" oninvalid="this.setCustomValidity('Nama gambar wajib diisi')" oninput="this.setCustomValidity('')" required>
                    </div>
                    <div class="mb-3">
                        <label for="image" class="form-label">Gambar</label>
                        <input type="file" class="form-control" id="image" name="file">
                        @if ($img->file)
                        <br>
                        <div class="rey-img"><img src="{{ url('/img/'.$img->file) }}" alt="{{ asset($img->name) }}">
                        </div>
                        @endif
                    </div>
                    <!-- Add more inputs as needed -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach
@endif

<!-- Create Gambart Modal -->
<div class="modal fade" id="createGambartModal" tabindex="-1" aria-labelledby="createGambartModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createGambartModalLabel">Tambah Gambar</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('images.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label for="name" class="form-label">Nama</label>
                        <input type="text" class="form-control" id="name" name="nama" placeholder="Masukan nama gambar" oninvalid="this.setCustomValidity('Nama gambar wajib diisi')" oninput="this.setCustomValidity('')" required>
                    </div>
                    <div class="mb-3">
                        <label for="image" class="form-label">Gambar</label>
                        <input type="file" class="form-control" id="image" name="file" oninvalid="this.setCustomValidity('File gambar wajib diisi')" oninput="this.setCustomValidity('')" required>
                    </div>
                    <!-- Add more inputs as needed -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection