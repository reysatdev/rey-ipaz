<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    
    public function index()
    {
        $products = Product::with('category', 'variant', 'gambar')->get();
        if ($products->isEmpty()) {
            return response()->json(['message' => 'Belum ada product'], 404);
        }
    
        // Mengedit data sebelum direspon
        $modifiedProducts = $products->map(function ($product) {
            $image = $product->gambar->file ?? null; // Ambil nama file gambar dari relasi
            $product->img = $image ? url('img/' . $image) : null;
            $product->url = url('product/' . $product->id);
            return $product;
        });
    
        return response()->json($modifiedProducts, 200);
    }
    public function show($id)
    {
        $product = Product::find($id);
        if (!$product) {
            return response()->json(['message' => 'Produk tidak ditemukan'], 404);
        }
        return response()->json($product, 200);
    }

    
}