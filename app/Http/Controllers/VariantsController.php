<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Variants;
use App\Models\Product;

class VariantsController extends Controller
{
    public function index()
    {
        $pageTitle = 'Variants | REY IPAZ SAMPLE';
        $variants = Variants::all();
        return view('variants.index', compact('pageTitle', 'variants'));
    }

    public function create()
    {
        return view('variants.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required'
        ], [
            'nama.required' => 'Nama harus diisi.'
        ]);

        $variant = new Variants();
        $variant->nama = $request->input('nama');
        // Assign other fields if needed

        $variant->save();

        return redirect()->route('variants.index')
            ->with('success', 'Varian berhasil dibuat.');
    }

    public function edit(Variants $variant)
    {
        return view('variants.edit', compact('variant'));
    }

    public function update(Request $request, Variants $variant)
    {
        $request->validate([
            'nama' => 'required'
        ], [
            'nama.required' => 'Nama harus diisi.'
        ]);

        $variant->nama = $request->input('nama');
        // Update other fields if needed

        $variant->save();

        return redirect()->route('variants.index')
            ->with('success', 'Varian berhasil diperbarui.');
    }

    public function destroy($id)
    {
        $variant = Variants::find($id);
        $isDependent = Product::where('variant_id', $variant->id)->exists();

        if ($isDependent) {
            return redirect()->route('variants.index')->with('error', 'Tidak dapat menghapus varian karena ada produk yang terkait.');
        }

        $variant->delete();

        return redirect()->route('variants.index')
            ->with('success', 'Varian berhasil dihapus.');
    }
}