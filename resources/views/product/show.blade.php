@extends('layouts.app')

@section('content')

<h2>Detail Produk</h2>
<div class="card">
    <img src="{{ $product->img }}" alt="Gambar Produk" class="w-100">
    <hr>
    <h2>{{ $product->nama }}</h2>
    <p>Harga: Rp {{ number_format($product->harga, 0, ',', '.') }}</p>
    <p>Kategori: {{ $product->category->nama }}</p>
    <p>Varian: {{ $product->variant->nama }}</p>

</div>

@endsection