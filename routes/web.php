<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\VariantsController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ImageController;

Route::get('/', function () {
    return view('index');
});

Route::resource('product', ProductController::class);
Route::resource('variants', VariantsController::class);
Route::resource('category', CategoryController::class);
Route::resource('images', ImageController::class);
Route::get('/products', 'ProductsController@index');
Route::get('/products/{id}', 'ProductsController@show');
Route::get('/product/{id}', 'ProductController@show')->name('product.show');