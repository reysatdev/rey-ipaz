<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;

class CategoryController extends Controller
{
    public function index()
    {
        $pageTitle = 'Kategori | REY IPAZ SAMPLE';
        $categories = Category::all();
        return view('category.index', compact('pageTitle', 'categories'));
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => ['required', 'filled']
        ], [
            'nama.required' => 'Nama harus diisi.'
        ]);

        $category = new Category();
        $category->nama = $request->input('nama');
        // Assign other fields if needed

        $category->save();

        return redirect()->route('category.index')
            ->with('success', 'Kategori berhasil dibuat.');
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('category.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => ['required', 'filled']
        ], [
            'nama.required' => 'Nama harus diisi.'
        ]);

        $category = Category::find($id);
        $category->nama = $request->input('nama');
        // Update other fields if needed

        $category->save();

        return redirect()->route('category.index')
            ->with('success', 'Kategori berhasil diperbarui.');
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        
        $isDependent = Product::where('category_id', $category->id)->exists();

        if ($isDependent) {
            return redirect()->route('category.index')->with('error', 'Tidak dapat menghapus kategori karena ada produk yang terkait.');
        }

        $category->delete();

        return redirect()->route('category.index')
            ->with('success', 'Kategori berhasil dihapus.');
    }
}