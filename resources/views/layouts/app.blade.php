<!DOCTYPE html>
<html lang="id">

<head>
  <title>{{ $pageTitle ?? 'REY IPAZ SAMPLE' }}</title>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
  <script src="{{ asset('js/jquery.min.js') }}"></script>
</head>

<body>
  <nav class="navbar navbar-expand-lg bg-rey">
    <div class="container">

      <a href="/" class="navbar-brand"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" class="logo-svg">
          <path
            d="M64 96c0-35.3 28.7-64 64-64H512c35.3 0 64 28.7 64 64V352H512V96H128V352H64V96zM0 403.2C0 392.6 8.6 384 19.2 384H620.8c10.6 0 19.2 8.6 19.2 19.2c0 42.4-34.4 76.8-76.8 76.8H76.8C34.4 480 0 445.6 0 403.2zM281 209l-31 31 31 31c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-48-48c-9.4-9.4-9.4-24.6 0-33.9l48-48c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9zM393 175l48 48c9.4 9.4 9.4 24.6 0 33.9l-48 48c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l31-31-31-31c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0z" />
        </svg> REY IPAZ SAMPLE</a>

      <!-- Start Hamburger menu -->
      <button class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <svg class="ham rg-ham" viewBox="0 0 100 100" width="80" onclick="this.classList.toggle('active')">
          <path class="line top"
            d="m 30,33 h 40 c 13.100415,0 14.380204,31.80258 6.899646,33.421777 -24.612039,5.327373 9.016154,-52.337577 -12.75751,-30.563913 l -28.284272,28.284272" />
          <path class="line middle"
            d="m 70,50 c 0,0 -32.213436,0 -40,0 -7.786564,0 -6.428571,-4.640244 -6.428571,-8.571429 0,-5.895471 6.073743,-11.783399 12.286435,-5.570707 6.212692,6.212692 28.284272,28.284272 28.284272,28.284272" />
          <path class="line bottom"
            d="m 69.575405,67.073826 h -40 c -13.100415,0 -14.380204,-31.80258 -6.899646,-33.421777 24.612039,-5.327373 -9.016154,52.337577 12.75751,30.563913 l 28.284272,-28.284272" />
        </svg>
      </button>
      <!-- End Hamburger menu -->

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ms-auto">
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/product') }}">Product</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/variants') }}">Variants</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/category') }}">Kategori</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/images') }}">Image</a>
          </li>
        </ul>
      </div>

    </div>
  </nav>

  <div class="container mt-5">
    @yield('content')
  </div>

  <footer class="py-3 my-4 mt-5">
    <ul class="nav justify-content-center border-bottom pb-3 mb-3 mt-5">
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/product') }}">Product</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/variants') }}">Variants</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/category') }}">Kategori</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/images') }}">Image</a>
      </li>
    </ul>
    <p class="text-center text-body-secondary">&copy; 2024 reysatdev@gmail.com</p>
  </footer>
  <script type="text/javascript" src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
</body>

</html>