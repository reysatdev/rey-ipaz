@extends('layouts.app')

@section('content')
<div class="text-center">
<h2>Contoh CURD Paling Sederhana Laravel by Reysat</h2>
</div>
<div class="ket">
Curd menggunakan Validasi paling dasar dua sisi (two-way validation) mengacu pada pendekatan validasi data di kedua sisi, yaitu di sisi klien (front-end) dan di sisi server (back-end).<br><br>
Validasi juga saya gunakan dari database, berdasarkan kunci ID, category_id, variant_id dan product_images id, jadi data dari kategori, variant dan gambar tidak bisa dihapus jika data sedang(masih digunakan) di data produk.<br><br>
Semua validasi hanya dasar sebagai contoh saja.<br><br>
Ada beberapa validasi yang dapat diterapkan untuk meningkatkan ketatnya validasi data. Berikut adalah beberapa contoh validasi yang lebih ketat:
<br><br>
Validasi pada sisi klien menggunakan pola regex: Selain validasi dasar seperti memeriksa apakah kolom input kosong, Anda juga dapat menggunakan pola regex (regular expression) untuk memvalidasi format yang lebih spesifik. Misalnya, Anda dapat menggunakan pola regex untuk memeriksa apakah input email memiliki format yang benar, apakah input nomor telepon mengikuti format tertentu, atau apakah input kode pos sesuai dengan format yang diharapkan.
<br><br>
Validasi pada sisi server menggunakan aturan bisnis yang lebih spesifik: Selain memeriksa tipe data dan kesesuaian umum, Anda dapat menerapkan validasi yang lebih ketat berdasarkan aturan bisnis atau persyaratan aplikasi. Misalnya, jika Anda memiliki persyaratan khusus untuk panjang minimal atau maksimal dari suatu input, Anda dapat memeriksa dan menolak data yang tidak memenuhi persyaratan tersebut.
<br><br>
Validasi lintas bidang (cross-field validation): Validasi lintas bidang melibatkan memeriksa hubungan atau ketergantungan antara beberapa bidang data. Contohnya, jika Anda memiliki formulir pendaftaran dengan bidang 'password' dan 'konfirmasi password', Anda dapat memeriksa apakah kedua bidang tersebut memiliki nilai yang sama. Jika tidak, maka validasi akan gagal.
<br><br>
Validasi lebih lanjut pada tingkat database: Selain validasi di sisi server, Anda juga dapat menerapkan validasi tambahan pada tingkat basis data. Misalnya, Anda dapat menggunakan constraint unik pada kolom basis data untuk memastikan bahwa suatu data harus unik di seluruh tabel.
<br><br>
Ingatlah bahwa validasi yang lebih ketat haruslah seimbang dengan pengalaman pengguna yang baik. Pastikan untuk memberikan pesan yang jelas dan informatif tentang kesalahan validasi kepada pengguna, sehingga mereka dapat memperbaiki masalah dengan mudah.
</div>
<div class="rey___div">
  <code>
  Contoh CRUD Product <a href="{{ url('/product') }}" target="_blank">Klik Disini</a>
  <br>
  Contoh CRUD Kategori <a href="{{ url('/category') }}" target="_blank">Klik Disini</a>
  <br>
  Contoh CRUD Gambar <a href="{{ url('/images') }}" target="_blank">Klik Disini</a>
  <br>
  Contoh CRUD Varian <a href="{{ url('/variants') }}" target="_blank">Klik Disini</a>
  <br>
  Url Api List Product: <a href="{{ url('/api/products') }}" target="_blank">{{ url('/') }}/api/products</a>
  <br>
  Contoh Url Api Detail Product (gunakan id product. contoh url dengan id product: 1): <a href="{{ url('/api/products/1') }}" target="_blank">{{ url('/api/products/1') }}</a>
  </code>
</div>

<h4>Contoh Cara Memanggil Api List Product</h4>

<div id="product-list"></div>

<div class="ket">Tampilan diatas, Menggunakan JavaScript murni (Vanilla JavaScript) tanpa bantuan dari jQuery. Jadi, tidak ada ketergantungan pada library jQuery dalam kode tersebut.

Anda dapat menggunakan kode tersebut tanpa harus memuat atau menggunakan jQuery. Ini adalah contoh JavaScript murni yang menggunakan metode `fetch()` untuk mengambil data dari API dan melakukan manipulasi DOM menggunakan metode JavaScript standar.<br><br>

Menampilkan data berdasarkan response dari API, contoh dibawah jika HTTP response status code bukan 200, maka tampilkan pesan, else tampilkan data.</div>
<pre class="pre">
<code>
  fetch('{{ url('/api/products') }}')
  .then(response => response.json())
  .then(data => {
    const productList = document.getElementById('product-list');

    if (data.response !== 200) {
      const listItem = document.createElement('ul');
      listItem.textContent = data.message;
      productList.appendChild(listItem);
    } else {
      data.forEach(product => {
        const listItem = document.createElement('ul');

        const nameElement = document.createElement('li');
        nameElement.textContent = 'Nama Produk: ' + product.nama;
        listItem.appendChild(nameElement);

        const priceElement = document.createElement('li');
        priceElement.textContent = 'Harga: ' + product.harga;
        listItem.appendChild(priceElement);

        const imgElement = document.createElement('li');
        imgElement.textContent = 'URL Image: ' + product.img;
        listItem.appendChild(imgElement);

        const urlElement = document.createElement('li');
        urlElement.textContent = 'URL Product: ' + product.url;
        listItem.appendChild(urlElement);

        productList.appendChild(listItem);
      });
    }
  })
  .catch(error => {
    console.error(error);
  });
</code>
</pre>

<script>
  fetch('{{ url('/api/products') }}')
  .then(response => response.json())
  .then(data => {
    const productList = document.getElementById('product-list');

    if (data.message) {
      const listItem = document.createElement('ul');
      listItem.textContent = data.message;
      productList.appendChild(listItem);
    } else {
      data.forEach(product => {
        const listItem = document.createElement('ul');

        const nameElement = document.createElement('li');
        nameElement.textContent = 'Nama Produk: ' + product.nama;
        listItem.appendChild(nameElement);

        const priceElement = document.createElement('li');
        priceElement.textContent = 'Harga: ' + product.harga;
        listItem.appendChild(priceElement);

        const imgElement = document.createElement('li');
        imgElement.textContent = 'URL Image: ' + product.img;
        listItem.appendChild(imgElement);

        const urlElement = document.createElement('li');
        urlElement.textContent = 'URL Product: ' + product.url;
        listItem.appendChild(urlElement);

        productList.appendChild(listItem);
      });
    }
  })
  .catch(error => {
    console.error(error);
  });
</script>

@endsection
