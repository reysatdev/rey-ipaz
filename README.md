# Rey Ipaz

Contoh CURD Simple Laravel. 

## Framework

Laravel Framework 10.43.0

## Instalasi

Berikut adalah langkah-langkah untuk menginstal dan menjalankan proyek ini di lingkungan lokal Anda.

1. Clone repositori ini ke direktori lokal Anda:

```
git clone https://gitlab.com/reysatdev/rey-ipaz.git
```

2. Masuk ke direktori proyek:

```
cd rey-ipaz
```

3. Salin file `.env.example` menjadi `.env`:

```
Linux: cp .env.example .env

Windows: copy .env.example .env
```

4. Atur konfigurasi database di file `.env`. Pastikan untuk mengisi informasi yang sesuai dengan lingkungan lokal Anda.

5. Jalankan perintah berikut untuk menginstal dependensi proyek:

```
composer install
```

6. Generate kunci aplikasi:

```
php artisan key:generate
```

7. Migrasikan tabel-tabel database:

```
php artisan migrate
```

8. Jalankan server pengembangan lokal:

```
php artisan serve

mipsasm
```

Server pengembangan akan berjalan di `http://localhost:8000`.

## Penggunaan

Berikut adalah langkah-langkah untuk menggunakan proyek ini:

Buka browser dan akses `http://localhost:8000`.

## Kontribusi

Kami menyambut kontribusi terbuka untuk proyek ini. Jika Anda ingin berkontribusi, ikuti langkah-langkah berikut:

1. Fork repositori ini.

2. Buat branch baru untuk fitur atau perbaikan yang akan Anda kerjakan:

```
git checkout -b fitur-baru
```

3. Lakukan perubahan yang diperlukan dan lakukan commit:

```
git commit -m "Menambahkan fitur baru"
```

4. Push branch Anda ke repositori forked:

```
git push origin fitur-baru

mipsasm

```

5. Buat pull request di repositori utama dan tunggu tinjauan dan persetujuan.


## Kontak

Jika Anda memiliki pertanyaan atau masalah terkait proyek ini, silakan hubungi saya melalui reysatdev@gmail.com.
